for user in $(git log --pretty="%ce%n" | sort | uniq);
do
   # print user, delta:  insertions - deletions
   # shellcheck disable=SC2046
   echo "$user" $(git log --author="$user" --pretty=tformat: --numstat | awk '{ delta+=$1-$2} END {printf "%s", delta}')
done