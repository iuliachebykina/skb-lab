
CREATE TABLE  IF NOT EXISTS reviewers
(
    project_id  INTEGER NOT NULL,
    reviewer_id INTEGER NOT NULL,
    status      VARCHAR(255),
    load        INTEGER,
    CONSTRAINT pk_reviewers PRIMARY KEY (project_id, reviewer_id)
);
CREATE TABLE IF NOT EXISTS titles
(
    project_id  INTEGER      NOT NULL,
    title       VARCHAR(255) NOT NULL,
    reviewer_id INTEGER,
    CONSTRAINT pk_titles PRIMARY KEY (project_id, title)
);
CREATE TABLE IF NOT EXISTS open_mrs
(
    project_id             INTEGER NOT NULL,
    open_merge_request_iid INTEGER NOT NULL,
    reviewer_id            INTEGER,
    CONSTRAINT pk_openmrs PRIMARY KEY (project_id, open_merge_request_iid)
);