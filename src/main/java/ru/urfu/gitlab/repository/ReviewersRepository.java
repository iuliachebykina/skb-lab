package ru.urfu.gitlab.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.urfu.gitlab.idClass.ReviewerId;
import ru.urfu.gitlab.entity.Reviewers;

import java.util.Optional;

public interface ReviewersRepository extends CrudRepository<Reviewers, ReviewerId> {

    @Query(value = "select reviewer_id  from Reviewers where status = 'AVAILABLE' and project_id = ?1  order by RANDOM() limit 1 ", nativeQuery = true)
    Optional<Integer> findAvailableReviewer(Integer projectId);

    @Query(value = "select reviewer_id  from Reviewers where status != 'NOT_AVAILABLE' and project_id = ?1 order by RANDOM() limit 1 ", nativeQuery = true)
    Optional<Integer> findWorkingReviewer(Integer projectId);

    @Query(value = "select project_id from reviewers where reviewer_id=?1", nativeQuery = true)
    Iterable<Integer> findAllProjectByReviewerId(Integer reviewerId);
}
