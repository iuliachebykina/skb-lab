package ru.urfu.gitlab.repository;

import org.springframework.data.repository.CrudRepository;
import ru.urfu.gitlab.idClass.OpenMrId;
import ru.urfu.gitlab.entity.OpenMrs;

public interface OpenMergeRequestsRepository extends CrudRepository<OpenMrs, OpenMrId> {
}
