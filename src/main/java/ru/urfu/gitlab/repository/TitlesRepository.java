package ru.urfu.gitlab.repository;

import org.springframework.data.repository.CrudRepository;
import ru.urfu.gitlab.idClass.TitleId;
import ru.urfu.gitlab.entity.Titles;

public interface TitlesRepository extends CrudRepository<Titles, TitleId> {
}
