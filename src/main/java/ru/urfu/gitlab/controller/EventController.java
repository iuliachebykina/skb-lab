package ru.urfu.gitlab.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.gitlab4j.api.GitLabApiException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import ru.urfu.gitlab.dto.ReviewerForm;
import ru.urfu.gitlab.service.MergeRequestHandlerService;
import ru.urfu.gitlab.dto.StatusForm;
import ru.urfu.gitlab.service.UpdateReviewerService;

import java.util.Map;


@Controller
public class EventController {

    private final MergeRequestHandlerService handlerMr;
    private final UpdateReviewerService updateReviewerService;
    private final static ObjectMapper mapper = new ObjectMapper();

    private static final String EVENT  = "Merge Request Hook";

    @Value("${app.secret_token}")
    private String secretToken;

    public EventController(MergeRequestHandlerService handler, UpdateReviewerService updateReviewerService) {
        this.handlerMr = handler;
        this.updateReviewerService = updateReviewerService;
    }

    @PostMapping("/webhooks")
    @ResponseBody
    public String mr(@RequestHeader MultiValueMap<String, String> headers, @RequestBody String body) throws JsonProcessingException, GitLabApiException {
        if (!headers.get("x-gitlab-token").get(0).equals(secretToken)) {
            return "error";
        }
        if (!headers.get("x-gitlab-event").get(0).equals(EVENT)) {
            return "error";
        }
        Map<String, Map<String, Object>> map = mapper.readValue(body, Map.class); //не понимаю, как исправить это предупреждение
        var iid = (Integer) map.get("object_attributes").get("iid");
        var action = map.get("object_attributes").get("action");
        var projectId = (Integer) map.get("project").get("id");
        if (action.equals("open")) {
            handlerMr.handleOpenMr(iid, projectId);
            return "";
        }
        if (action.equals("close") || action.equals("merge")) {
            handlerMr.handleClosedMr(iid, projectId);
            return "";
        }
        if (action.equals("reopen")) {
            handlerMr.checkReopenMr(iid, projectId);
            return "";
        }
        if (action.equals("update")) {
            handlerMr.changeReviewer(iid, projectId);
            return "";
        }
        return "";
    }


    @GetMapping("/changeStatus")
    public String changeStatus() {
        return "changeStatus";
    }

    @PostMapping("/changeStatus")
    public String changeStatus(@ModelAttribute StatusForm statusForm) throws GitLabApiException {
        String username = statusForm.getUsername();
        String status = statusForm.getStatus();
        Integer projectId = statusForm.getProjectId();
        Boolean isAllProject = statusForm.getIsAllProject();
        updateReviewerService.changeStatus(username, status, projectId, isAllProject);
        return "changeStatus";
    }


    @GetMapping("/addReviewer")
    public String addReviewer() {
        return "addReviewer";
    }

    @PostMapping("/addReviewer")
    public String addReviewer(@ModelAttribute ReviewerForm reviewerForm) throws GitLabApiException {
        String username = reviewerForm.getUsername();
        Integer projectId = reviewerForm.getProjectId();
        updateReviewerService.addReviewer(username, projectId);
        return "addReviewer";
    }
}
