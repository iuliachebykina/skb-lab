package ru.urfu.gitlab.service;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.urfu.gitlab.idClass.ReviewerId;
import ru.urfu.gitlab.idClass.TitleId;
import ru.urfu.gitlab.entity.Reviewers;
import ru.urfu.gitlab.entity.Titles;
import ru.urfu.gitlab.repository.ReviewersRepository;
import ru.urfu.gitlab.enums.Status;
import ru.urfu.gitlab.repository.TitlesRepository;

import javax.transaction.Transactional;

@Data
@Service
public class UpdateDataBaseService {
    private final ReviewersRepository reviewersRepository;
    private final TitlesRepository titlesRepository;
    @Value("${app.max_load}")
    private Integer maxLoad;
    private final static Logger log = LoggerFactory.getLogger(UpdateDataBaseService.class);

    @Transactional
    public void addReviewer(Integer reviewerId, Integer projectId, Integer load) {

        var status = load >= maxLoad ? Status.OVERBURDENED : Status.AVAILABLE;
        var reviewer = new Reviewers(reviewerId, projectId,  status, load);
        reviewersRepository.save(reviewer);
        log.info("Reviewer (id: {}) added for project (id: {})", reviewerId, projectId);

    }

    @Transactional
    public void addLoad(Integer reviewerId, Integer projectId) {
        var reviewer = reviewersRepository.findById(new ReviewerId(projectId, reviewerId));
        if (reviewer.isEmpty()) {
            return;
        }
        var r = reviewer.get();
        r.setLoad(r.getLoad() + 1);
        if (r.getLoad() >= maxLoad) {
            r.setStatus(Status.OVERBURDENED.toString());
        }
        reviewersRepository.save(r);
        log.info("Reviewer (id: {}) load added for project (id: {})", reviewerId, projectId);
    }

    @Transactional
    public void removeLoad(Integer reviewerId, Integer projectId) {
        var reviewer = reviewersRepository.findById(new ReviewerId(projectId, reviewerId));
        if (reviewer.isEmpty()) {
            return;
        }
        var r = reviewer.get();
        r.setLoad(r.getLoad() - 1);
        if (r.getLoad() < maxLoad) {
            r.setStatus(Status.AVAILABLE.toString());
        }
        reviewersRepository.save(r);
        log.info("Reviewer (id: {}) load reduced for project (id: {})", reviewerId, projectId);
    }

    @Transactional
    public void changeStatus(Integer reviewerId, String status, Iterable<Integer> projectIds) {
        for (Integer projectId:
             projectIds) {
            var optionalReviewer = reviewersRepository.findById(new ReviewerId(projectId, reviewerId));
            if (optionalReviewer.isEmpty()) {
                continue;
            }
            var reviewer = optionalReviewer.get();
            reviewer.setStatus(status);
            reviewer.setLoad(0);
            reviewersRepository.save(reviewer);
            log.info("Status changed for Reviewer (id: {}) to {} for project (id: {})", reviewer.getReviewerId(), status, projectId);
        }
    }

    @Transactional
    public void addTitle(String title, Integer reviewerId, Integer projectId){
        var titleToReviewer = titlesRepository.findById(new TitleId(projectId, title ));
        if(titleToReviewer.isPresent()){
            var t = titleToReviewer.get();
            t.setReviewerId(reviewerId);
            titlesRepository.save(t);
        } else {
            titlesRepository.save(new Titles(projectId, title, reviewerId));
        }
    }

}
