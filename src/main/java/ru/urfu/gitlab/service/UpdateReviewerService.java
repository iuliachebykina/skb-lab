package ru.urfu.gitlab.service;

import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.MergeRequest;
import org.springframework.stereotype.Service;
import ru.urfu.gitlab.enums.Status;
import ru.urfu.gitlab.repository.ReviewersRepository;

import java.util.List;

@Service
public class UpdateReviewerService {
    private final GitLabService gitLabService;
    private final UpdateDataBaseService updateDB;
    private final MergeRequestHandlerService handlerMr;
    private final ReviewersRepository reviewersRepository;

    public UpdateReviewerService(GitLabService gitLabService, UpdateDataBaseService updateDB,
                                 MergeRequestHandlerService handlerMr, ReviewersRepository reviewersRepository) {
        this.gitLabService = gitLabService;
        this.updateDB = updateDB;
        this.handlerMr = handlerMr;
        this.reviewersRepository = reviewersRepository;
    }

    public void addReviewer(String username, Integer projectId) throws GitLabApiException {
        var id = gitLabService.getUserIdByUsername(username);
        if (id == null || !gitLabService.isMember(id, projectId)) {
            return;
        }
        updateDB.addReviewer(id, projectId, 0);
    }

    public void changeStatus(String username, String status, Integer projectId, boolean isAllProject) throws GitLabApiException {
        var id = gitLabService.getUserIdByUsername(username);
        if (id == null) {
            return;
        }
        var projects = isAllProject? reviewersRepository.findAllProjectByReviewerId(id): List.of(projectId);
        updateDB.changeStatus(id, status, projects);
        if (!status.equals(Status.NOT_AVAILABLE.toString())) {
            return;
        }
        var mrs = gitLabService.getMrForChangeReviewer(id, projectId);
        for (MergeRequest mr :
                mrs) {
            handlerMr.handleOpenMr(mr.getIid(), projectId);
        }
    }
}
