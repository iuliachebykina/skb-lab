package ru.urfu.gitlab.service;

import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class GitLabService {
    private final GitLabApi gitLabApi;

    private final static Logger log = LoggerFactory.getLogger(GitLabService.class);

    public GitLabService(GitLabApi gitLabApi) {
        this.gitLabApi = gitLabApi;
    }

    public Project getProject(Integer projectId) throws GitLabApiException {
        return gitLabApi.getProjectApi().getProject(projectId);
    }

    public String getProjectUrl(Integer projectIid) throws GitLabApiException {
        var project = getProject(projectIid);
        return project.getWebUrl();
    }

    public String getProjectName(Integer projectIid) throws GitLabApiException {
        var project = getProject(projectIid);
        return project.getName().toLowerCase(Locale.ROOT);
    }

    public List<Member> getMembers(Integer projectIid) throws GitLabApiException {
        var project = getProject(projectIid);
        return gitLabApi.getProjectApi().getMembers(project.getId());
    }

    public List<MergeRequest> getMergeRequests(Constants.MergeRequestState state, Integer projectIid) throws GitLabApiException {
        var project = getProject(projectIid);
        return gitLabApi.getMergeRequestApi().getMergeRequests(project.getId(), state);
    }

    public List<MergeRequest> getOpenedMergeRequestsWithoutReviewer(Integer projectIid) throws GitLabApiException {
        var project = getProject(projectIid);
        return gitLabApi.getMergeRequestApi()
                .getMergeRequests(project.getId(), Constants.MergeRequestState.OPENED)
                .stream()
                .filter(t -> t.getReviewers().isEmpty())
                .collect(Collectors.toList());

    }

    public List<MergeRequest> getOpenedMergeRequestsWithReviewer(Integer projectIid) throws GitLabApiException {
        var project = getProject(projectIid);
        return gitLabApi.getMergeRequestApi()
                .getMergeRequests(project.getId(), Constants.MergeRequestState.OPENED)
                .stream()
                .filter(t -> !t.getReviewers().isEmpty() && !t.getWorkInProgress())
                .collect(Collectors.toList());
    }

    public MergeRequest getMergeRequest(Integer iid, Integer projectIid) throws GitLabApiException {
        var project = getProject(projectIid);
        return gitLabApi.getMergeRequestApi().getMergeRequest(project.getId(), iid);
    }

    public void addReviewer(Integer iid, Integer reviewerId, Integer projectIid) throws GitLabApiException {
        var project = getProject(projectIid);
        MergeRequestParams params = new MergeRequestParams()
                .withReviewerIds(List.of(reviewerId));
        gitLabApi.getMergeRequestApi().updateMergeRequest(project.getId(), iid, params);
    }

    public Integer getUserIdByEmail(String email) {
        try {
            return gitLabApi.getUserApi().getUserByEmail(email).getId();
        } catch (Exception e) {
            log.warn("User by email not found");
            return null;
        }

    }

    public Integer getUserIdByUsername(String username) {
        try {
            return gitLabApi.getUserApi().getUser(username).getId();
        } catch (Exception e) {
            log.warn("User by username not found");
            return null;
        }
    }

    public Boolean isMember(Integer memberId, Integer projectIid) throws GitLabApiException {
        var project = getProject(projectIid);
        return gitLabApi.getProjectApi().getMember(project.getId(), memberId).getAccessLevel().value >= AccessLevel.MAINTAINER.value;
    }


    public List<MergeRequest> getMrForChangeReviewer(Integer id, Integer projectIid) throws GitLabApiException {
        var result = new ArrayList<MergeRequest>();
        for (MergeRequest mr :
                getMergeRequests(Constants.MergeRequestState.OPENED, projectIid)) {
            if (!Objects.requireNonNull(mr.getReviewers()).isEmpty() && mr.getReviewers().get(0).getId().equals(id)) {
                mr.setReviewers(new ArrayList<>());
                result.add(mr);
            }
        }
        return result;
    }

}
