package ru.urfu.gitlab.service;


import org.gitlab4j.api.GitLabApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


import java.io.*;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

@Service
public class GitCommandService {
    private final ProcessBuilder processBuilder = new ProcessBuilder();
    private final static Logger log = LoggerFactory.getLogger(GitCommandService.class);
    private final GitLabService gitLabService;
    private final String repoDir;
    private final String script;


    public GitCommandService( GitLabService gitLabService) {
        this.gitLabService = gitLabService;
        this.repoDir = new File(new File("").getAbsolutePath()).getParent();
        this.script = Objects.requireNonNull(getClass().getResource("/script.sh")).getPath();
    }

    public void cloneRepo(Integer projectId) throws GitLabApiException {
        var url = gitLabService.getProjectUrl(projectId).toLowerCase(Locale.ROOT);
        var clone = "git clone " + url + ".git";
        Process process;
        try {
            process = processBuilder
                    .command("sh", "-c", clone)
                    .directory(new File(repoDir))
                    .start();
            try {
                process.waitFor();
                log.info("Project (url: " + url + ") cloned");
            } catch (InterruptedException e) {
                log.warn(e.getMessage());
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    void changeBranch(String branch, String projectDir) {

        try {
            Process process = processBuilder
                    .command("sh", "-c", "git checkout " + branch)
                    .directory(new File(projectDir))
                    .start();
            try {
                process.waitFor();
                log.info("Change branch to " + branch);
            } catch (InterruptedException e) {
                log.error(e.getMessage());
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }


    }

    public void getRulesForScript(Integer projectId) throws GitLabApiException { // ничего лучше в голову не приходит, но, вроде, работает
        var projectDir = repoDir + "/" + gitLabService.getProjectName(projectId);
        try {
            Process process = processBuilder
                    .command("chmod", "+x", script)
                    .directory(new File(projectDir))
                    .start();
            try {
                process.waitFor();
                log.info("Rules received");
            } catch (InterruptedException e) {
                log.error(e.getMessage());
            }

        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    public HashMap<String, Integer> getContributionStatistics(String branch, Integer projectId) throws GitLabApiException {
        var projectDir = repoDir + "/" + gitLabService.getProjectName(projectId);
        changeBranch(branch, projectDir);
        HashMap<String, Integer> map = new HashMap<>();
        try {
            Process process = processBuilder
                    .command(script)
                    .directory(new File(projectDir))
                    .start();
            StreamGobbler streamGobbler =
                    new StreamGobbler(process.getInputStream(), t -> {
                        var arr = t.split(" ");
                        var user = arr[0];
                        var contr = Integer.parseInt(arr[1]);
                        map.put(user, contr);
                    });
            Executors.newSingleThreadExecutor().submit(streamGobbler);
            try {
                process.waitFor();
                if (map.isEmpty()) {
                    log.info("Member contribution statistics not found");
                } else {
                    log.info("Member contribution statistics found");
                }
            } catch (InterruptedException e) {
                log.error(e.getMessage());
            }
            return map;
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return null;
    }

}

class StreamGobbler implements Runnable {
    private final InputStream inputStream;
    private final Consumer<String> consumer;

    public StreamGobbler(InputStream inputStream, Consumer<String> consumer) {
        this.inputStream = inputStream;
        this.consumer = consumer;
    }

    @Override
    public void run() {
        new BufferedReader(new InputStreamReader(inputStream)).lines()
                .forEach(consumer);
    }
}