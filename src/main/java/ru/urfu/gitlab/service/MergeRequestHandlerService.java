package ru.urfu.gitlab.service;

import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.MergeRequest;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.urfu.gitlab.idClass.OpenMrId;
import ru.urfu.gitlab.idClass.ReviewerId;
import ru.urfu.gitlab.idClass.TitleId;
import ru.urfu.gitlab.entity.OpenMrs;
import ru.urfu.gitlab.repository.OpenMergeRequestsRepository;
import ru.urfu.gitlab.repository.ReviewersRepository;
import ru.urfu.gitlab.enums.Status;
import ru.urfu.gitlab.repository.TitlesRepository;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


@Service
public class MergeRequestHandlerService {

    private final GitLabService gitLabService;
    private final ReviewersRepository reviewersRepository;
    private final UpdateDataBaseService updateDataBase;
    private final GitCommandService gitCommandService;
    private final TitlesRepository titlesRepository;
    private final OpenMergeRequestsRepository openMergeRequestsRepository;
    private final static Logger log = LoggerFactory.getLogger(MergeRequestHandlerService.class);

    public MergeRequestHandlerService(GitLabService gitLabService, ReviewersRepository reviewersRepository,
                                      UpdateDataBaseService updateDataBase, GitCommandService gitCommandService,
                                      TitlesRepository titlesRepository, OpenMergeRequestsRepository openMergeRequestsRepository) {
        this.gitLabService = gitLabService;
        this.reviewersRepository = reviewersRepository;
        this.updateDataBase = updateDataBase;
        this.gitCommandService = gitCommandService;
        this.titlesRepository = titlesRepository;
        this.openMergeRequestsRepository = openMergeRequestsRepository;
    }

    public void handleOpenMr(Integer mrIid, Integer projectId) throws GitLabApiException {
        var mr = gitLabService.getMergeRequest(mrIid, projectId);
        if (!mr.getReviewers().isEmpty())
            return;

        var reviewerId = findReviewerId(mr, projectId);
        if (reviewerId != null) {
            updateDataBase.addLoad(reviewerId, projectId);
            gitLabService.addReviewer(mrIid, reviewerId, projectId);
            addOpenMergeRequest(mr.getIid(), reviewerId, projectId);
            log.info("Open/modified Merge Request (iid: {}) processed", mr.getIid());
        } else {
            log.info("Not found working reviewers for Merge Request (iid: {})", mr.getIid());
        }

    }

    public void handleClosedMr(Integer mrIid, Integer projectId) throws GitLabApiException {
        var mr = gitLabService.getMergeRequest(mrIid, projectId);
        var reviewers = mr.getReviewers();
        if (reviewers.isEmpty()) {
            return;
        }
        addTitleReviewerToMap(mr, projectId);
        updateDataBase.removeLoad(reviewers.get(0).getId(), projectId);
        openMergeRequestsRepository.delete(new OpenMrs(projectId, mrIid, reviewers.get(0).getId()));
        log.info("Close/merge Merge Request (iid: {}) processed", mr.getIid());
    }

    String parseTitle(String title) {
        Matcher m = Pattern.compile("\\[([^]]+)]").matcher(title);
        if (m.find()) {
            return m.group(1).toLowerCase(Locale.ROOT);
        }
        return title;
    }

    public void changeReviewer(Integer mrIid, Integer projectId) throws GitLabApiException {
        var mr = gitLabService.getMergeRequest(mrIid, projectId);
        if (mr.getReviewers().isEmpty()) {
            var reviewerId = openMergeRequestsRepository.findById(new OpenMrId(projectId, mrIid));
            if (reviewerId.isEmpty()) {
                return;
            }
            openMergeRequestsRepository.save(new OpenMrs(projectId, mrIid, null));
            updateDataBase.removeLoad(reviewerId.get().getReviewerId(), projectId);
            log.info("Remove reviewer (id: {}) for mr (iid: {})", reviewerId, mrIid);

        } else {
            var reviewerId = mr.getReviewers().get(0).getId();
            var oldReviewerId = openMergeRequestsRepository.findById(new OpenMrId(projectId, mrIid));
            if (oldReviewerId.isEmpty()) {
                return;
            }
            if (reviewerId.equals(oldReviewerId.get().getReviewerId())) {
                return;
            }
            updateDataBase.addLoad(reviewerId, projectId);
            openMergeRequestsRepository.save(new OpenMrs(projectId, mrIid, reviewerId));
            if (oldReviewerId.get().getReviewerId() == null) {
                log.info("Add reviewer (id:{}) for mr (iid: {})", reviewerId, mrIid);
            } else {
                updateDataBase.removeLoad(oldReviewerId.get().getReviewerId(), projectId);
                log.info("Change reviewer for mr (iid: {}) from id:{} to id:{}", mrIid, oldReviewerId, reviewerId);
            }


        }

    }

    public void addTitleReviewerToMap(MergeRequest mr, Integer projectId) {
        if (mr.getReviewers().isEmpty()) {
            return;
        }
        var title = parseTitle(mr.getTitle());
        var reviewerId = mr.getReviewers().get(0).getId();
        updateDataBase.addTitle(title, reviewerId, projectId);
    }

    public void addOpenMergeRequest(Integer mrIid, Integer reviewerId, Integer projectId) {
        openMergeRequestsRepository.save(new OpenMrs(projectId, mrIid, reviewerId));
    }

    public void checkReopenMr(Integer mrIid, Integer projectId) throws GitLabApiException {
        var mr = gitLabService.getMergeRequest(mrIid, projectId);
        var reviewers = mr.getReviewers();
        if (!reviewers.isEmpty()) {
            var reviewer = reviewersRepository.findById(new ReviewerId(projectId, reviewers.get(0).getId()));
            if (reviewer.isPresent()) {
                if (reviewer.get().getStatus().equals(Status.AVAILABLE.toString())) {
                    var reviewerId = reviewer.get().getReviewerId();
                    updateDataBase.addLoad(reviewerId, projectId);
                    addOpenMergeRequest(mr.getIid(), reviewerId, projectId);
                    return;
                }
            }
        }
        handleOpenMr(mrIid, projectId);
    }


    Integer findReviewerId(MergeRequest mr, Integer projectId) throws GitLabApiException {

        Integer reviewerId = null;
        var mrIid = mr.getIid();
        var reviewerIdClosedMr = titlesRepository.findById(new TitleId(projectId, parseTitle(mr.getTitle())));
        if (reviewerIdClosedMr.isPresent()) {
            var reviewer = reviewerIdClosedMr.get();
            if (reviewersRepository.findById(new ReviewerId(projectId, reviewer.getReviewerId())).isPresent()) {
                reviewerId = reviewer.getReviewerId();
                log.info("Find closed mr with reviewer for mr (iid: {})", mrIid);
            }
        } else {
            var map = getMapOfContributions(mr, projectId);
            if (map != null) {
                var sortedMap = map.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).collect(Collectors.toMap(
                        Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, HashMap::new));
                for (String email :
                        sortedMap.keySet()) {
                    var id = gitLabService.getUserIdByEmail(email);
                    if (id == null) {
                        continue;
                    }
                    var user = reviewersRepository.findById(new ReviewerId(projectId, reviewerId));
                    if (user.isPresent()) {
                        if (user.get().getStatus().equals(Status.AVAILABLE.toString())) {
                            reviewerId = user.get().getReviewerId();
                            log.info("Find reviewer for mr (iid: {}) from contributions", mrIid);
                            break;
                        }
                    }
                }
            }

            if (reviewerId == null) {
                var availableReviewer = reviewersRepository.findAvailableReviewer(projectId);
                if (availableReviewer.isPresent()) {
                    reviewerId = availableReviewer.get();
                    log.info("Find reviewer for mr (iid: {}) from available reviewers", mrIid);
                }
            }
            if (reviewerId == null) {
                var workingReviewer = reviewersRepository.findWorkingReviewer(projectId);
                if (workingReviewer.isPresent()) {
                    reviewerId = workingReviewer.get();
                    log.info("Find reviewer for mr (iid: {}) from working reviewers", mrIid);
                }
            }

        }
        return reviewerId;
    }

    HashMap<String, Integer> getMapOfContributions(@NotNull MergeRequest mr, Integer projectId) throws GitLabApiException {
        var branch = mr.getSourceBranch();
        return gitCommandService.getContributionStatistics(branch, projectId);

    }

}