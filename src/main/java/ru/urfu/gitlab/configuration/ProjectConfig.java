package ru.urfu.gitlab.configuration;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.ProjectFilter;
import org.gitlab4j.api.models.Visibility;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;


@Configuration
public class ProjectConfig {

    private final ConfigProperties configProperties;

    public ProjectConfig(ConfigProperties configProperties) {
        this.configProperties = configProperties;
    }

//    @Bean //список проектов из yml
//    public List<Project> projects(GitLabApi gitLabApi) throws GitLabApiException {
//        var list = new ArrayList<Project>();
//        for (Integer projectId:
//                configProperties.getProjects()) {
//            list.add(gitLabApi.getProjectApi().getProject(projectId));
//        }
//        return list;
//    }

    @Bean // все публичные проекты юзера
    public List<Project> projects(GitLabApi gitLabApi) throws GitLabApiException {
        var filter = new ProjectFilter().withVisibility(Visibility.PUBLIC);
        return gitLabApi.getProjectApi().getUserProjects(configProperties.getUserId(), filter);
    }

    @Bean
    public GitLabApi gitLabApi() {
        return new GitLabApi(configProperties.getHost(), configProperties.getToken());
    }
}
