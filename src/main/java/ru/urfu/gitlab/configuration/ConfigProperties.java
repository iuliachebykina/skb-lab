package ru.urfu.gitlab.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "app")
@Data
public class ConfigProperties {

    private String host;
    private String token;
    private Integer userId;
    private List<Integer> projects;
}
