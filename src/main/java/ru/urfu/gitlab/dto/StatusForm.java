package ru.urfu.gitlab.dto;

import lombok.Data;

@Data
public class StatusForm {
    private String username;
    private String status;
    private Integer projectId;
    private Boolean isAllProject;
}
