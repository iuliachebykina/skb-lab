package ru.urfu.gitlab.dto;

import lombok.Data;

@Data
public class ReviewerForm {
    private String username;
    private Integer projectId;
}
