package ru.urfu.gitlab.events;

import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.AccessLevel;
import org.gitlab4j.api.models.Member;
import org.gitlab4j.api.models.MergeRequest;
import org.gitlab4j.api.models.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.urfu.gitlab.service.GitLabService;
import ru.urfu.gitlab.service.MergeRequestHandlerService;
import ru.urfu.gitlab.service.UpdateDataBaseService;

import java.util.List;


public class DataBaseFillerEvent {
    private final GitLabService gitLabService;
    private final MergeRequestHandlerService handlerMr;
    private final UpdateDataBaseService updateDataBase;
    private final List<Project> projects;

    private final static Logger log = LoggerFactory.getLogger(DataBaseFillerEvent.class);

    public DataBaseFillerEvent(GitLabService gitLabService, MergeRequestHandlerService handlerMr,
                               UpdateDataBaseService updateDataBase, List<Project> projects) {
        this.gitLabService = gitLabService;
        this.handlerMr = handlerMr;
        this.updateDataBase = updateDataBase;
        this.projects = projects;
    }


    public void fillDataBase() throws GitLabApiException {
        for (Project project:
             projects) {
            var projectId = project.getId();
            var members = gitLabService.getMembers(projectId);
            if (members.isEmpty()) {
                return;
            }
            var mrs = gitLabService.getOpenedMergeRequestsWithReviewer(projectId);

            for (Member member : members) {
                if (member.getAccessLevel().value < AccessLevel.MAINTAINER.value) {
                    continue;
                }
                var id = member.getId();
                Integer load = 0;
                for (MergeRequest mr :
                        mrs) {
                    if (mr.getReviewers().get(0).getId().equals(id)) {
                        load++;
                        handlerMr.addOpenMergeRequest(mr.getIid(), id, projectId);

                    }
                }
                updateDataBase.addReviewer(id, projectId, load);
            }
            log.info("All project (id: {}) members added", projectId);

        }


    }


}
