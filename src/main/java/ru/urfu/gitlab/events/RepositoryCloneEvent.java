package ru.urfu.gitlab.events;

import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;
import ru.urfu.gitlab.service.GitCommandService;

import java.util.List;

public class RepositoryCloneEvent {

    private final GitCommandService gitCommandService;
    private final List<Project> projects;

    public RepositoryCloneEvent(GitCommandService gitCommandService, List<Project> projects) {
        this.gitCommandService = gitCommandService;
        this.projects = projects;
    }

    public void cloneProject() throws GitLabApiException {
        for (Project project:
             projects) {
            gitCommandService.cloneRepo(project.getId());
            gitCommandService.getRulesForScript(project.getId());
        }
    }
}
