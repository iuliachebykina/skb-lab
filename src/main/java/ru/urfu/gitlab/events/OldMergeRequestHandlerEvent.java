package ru.urfu.gitlab.events;

import org.gitlab4j.api.Constants;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.MergeRequest;
import org.gitlab4j.api.models.Project;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.urfu.gitlab.service.GitLabService;
import ru.urfu.gitlab.service.MergeRequestHandlerService;

import java.util.List;

public class OldMergeRequestHandlerEvent{
    private final GitLabService gitLabService;
    private final MergeRequestHandlerService handler;
    private final static Logger log = LoggerFactory.getLogger(OldMergeRequestHandlerEvent.class);

    public OldMergeRequestHandlerEvent(GitLabService gitLabService, MergeRequestHandlerService handler) {
        this.gitLabService = gitLabService;
        this.handler = handler;
    }

    public void processedMissedMergeRequests(List<Project> projects) throws GitLabApiException {
        for (Project project:
                projects) {
            var projectId = project.getId();
            var mrs = gitLabService.getOpenedMergeRequestsWithoutReviewer(projectId);
            for (MergeRequest mr : mrs) {
                handler.handleOpenMr(mr.getIid(), projectId);
            }

            log.info("All missed open merge requests processed for project (id: {})", projectId);
        }


    }

    public void processedClosedMergeRequests(List<Project> projects) throws GitLabApiException {
        for (Project project:
             projects) {
            var projectId = project.getId();
            var mrs = gitLabService.getMergeRequests(Constants.MergeRequestState.CLOSED, projectId);
            for (MergeRequest mr :
                    mrs) {
                handler.addTitleReviewerToMap(mr, projectId);
            }
            log.info("All closed merge requests processed for project (id: {})", projectId);
        }

    }
}
