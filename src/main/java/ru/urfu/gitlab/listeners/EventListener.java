package ru.urfu.gitlab.listeners;

import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Project;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import ru.urfu.gitlab.events.OldMergeRequestHandlerEvent;
import ru.urfu.gitlab.events.RepositoryCloneEvent;
import ru.urfu.gitlab.service.GitCommandService;
import ru.urfu.gitlab.events.DataBaseFillerEvent;
import ru.urfu.gitlab.service.GitLabService;
import ru.urfu.gitlab.service.MergeRequestHandlerService;
import ru.urfu.gitlab.service.UpdateDataBaseService;

import java.util.List;

@Component
public class EventListener implements ApplicationListener<ApplicationReadyEvent> {

    private final static Logger log = LoggerFactory.getLogger(EventListener.class);
    private final GitLabService gitLabService;
    private final UpdateDataBaseService updateDataBase;
    private final MergeRequestHandlerService handler;
    private final GitCommandService gitCommandService;
    private final List<Project> projects;

    public EventListener(GitLabService gitLabService, UpdateDataBaseService updateDataBase, MergeRequestHandlerService handler, GitCommandService gitCommandService, List<Project> projects) {
        this.gitLabService = gitLabService;
        this.updateDataBase = updateDataBase;
        this.handler = handler;
        this.gitCommandService = gitCommandService;
        this.projects = projects;
    }

    @Override
    public void onApplicationEvent(@NotNull ApplicationReadyEvent event) {
        var fillerDB = new DataBaseFillerEvent(gitLabService, handler, updateDataBase, projects);
        var handlerOldMr = new OldMergeRequestHandlerEvent(gitLabService, handler);
        var cloneRepo = new RepositoryCloneEvent(gitCommandService, projects);
        try {
            fillerDB.fillDataBase();
            cloneRepo.cloneProject();
            handlerOldMr.processedMissedMergeRequests(projects);
            handlerOldMr.processedClosedMergeRequests(projects);
        } catch (GitLabApiException e) {
            log.error(e.getMessage());
        }

    }


}
