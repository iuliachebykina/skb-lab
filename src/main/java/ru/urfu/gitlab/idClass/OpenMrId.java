package ru.urfu.gitlab.idClass;

import lombok.RequiredArgsConstructor;
import org.hibernate.Hibernate;

import java.io.Serializable;
import java.util.Objects;

@RequiredArgsConstructor
public class OpenMrId implements Serializable {
    Integer projectId;
    Integer openMergeRequestIid;

    public OpenMrId(Integer projectId, Integer openMergeRequestIid){
        this.projectId =projectId;
        this.openMergeRequestIid = openMergeRequestIid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        OpenMrId that = (OpenMrId) o;

        if (!Objects.equals(projectId, that.projectId)) return false;
        return Objects.equals(openMergeRequestIid, that.openMergeRequestIid);
    }

    @Override
    public int hashCode() {
        int result = Objects.hashCode(projectId);
        result = 31 * result + (Objects.hashCode(openMergeRequestIid));
        return result;
    }
}
