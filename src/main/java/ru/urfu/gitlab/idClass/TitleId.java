package ru.urfu.gitlab.idClass;

import lombok.RequiredArgsConstructor;
import org.hibernate.Hibernate;

import java.io.Serializable;
import java.util.Objects;

@RequiredArgsConstructor
public class TitleId implements Serializable {
    String title;
    Integer projectId;

    public TitleId(Integer projectId, String title){
        this.projectId = projectId;
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        TitleId titleId = (TitleId) o;

        if (!Objects.equals(title, titleId.title)) return false;
        return Objects.equals(projectId, titleId.projectId);
    }

    @Override
    public int hashCode() {
        int result = Objects.hashCode(title);
        result = 31 * result + (Objects.hashCode(projectId));
        return result;
    }
}
