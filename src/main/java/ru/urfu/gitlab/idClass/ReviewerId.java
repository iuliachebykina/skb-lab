package ru.urfu.gitlab.idClass;

import lombok.RequiredArgsConstructor;
import org.hibernate.Hibernate;

import java.io.Serializable;
import java.util.Objects;

@RequiredArgsConstructor
public class ReviewerId implements Serializable {
    Integer projectId;
    Integer reviewerId;

    public ReviewerId(Integer projectId, Integer reviewerId) {
        this.reviewerId = reviewerId;
        this.projectId = projectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ReviewerId that = (ReviewerId) o;

        if (!Objects.equals(projectId, that.projectId)) return false;
        return Objects.equals(reviewerId, that.reviewerId);
    }

    @Override
    public int hashCode() {
        int result = Objects.hashCode(projectId);
        result = 31 * result + (Objects.hashCode(reviewerId));
        return result;
    }
}
