package ru.urfu.gitlab.entity;

import lombok.*;
import org.hibernate.Hibernate;
import ru.urfu.gitlab.idClass.ReviewerId;
import ru.urfu.gitlab.enums.Status;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@IdClass(ReviewerId.class)
public class Reviewers {
    @Id
    private Integer projectId;
    @Id
    private Integer reviewerId;
    private String status;
    private Integer load;


    public Reviewers(Integer reviewerId, Integer projectId, Status status, int load) {
        this.reviewerId = reviewerId;
        this.projectId = projectId;
        this.status = status.toString();
        this.load = load;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Reviewers reviewers = (Reviewers) o;

        if (!Objects.equals(projectId, reviewers.projectId)) return false;
        return Objects.equals(reviewerId, reviewers.reviewerId);
    }

    @Override
    public int hashCode() {
        int result = Objects.hashCode(projectId);
        result = 31 * result + (Objects.hashCode(reviewerId));
        return result;
    }
}

