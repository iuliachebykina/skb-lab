package ru.urfu.gitlab.entity;

import lombok.*;
import org.hibernate.Hibernate;
import ru.urfu.gitlab.idClass.TitleId;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@IdClass(TitleId.class)
public class Titles {
    @Id
    private Integer projectId;
    @Id
    private String title;
    private Integer reviewerId;

    public Titles(Integer projectId, String title, Integer reviewerId) {
        this.projectId = projectId;
        this.title =title;
        this.reviewerId =reviewerId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Titles titles = (Titles) o;

        if (!Objects.equals(projectId, titles.projectId)) return false;
        return Objects.equals(title, titles.title);
    }

    @Override
    public int hashCode() {
        int result = Objects.hashCode(projectId);
        result = 31 * result + (Objects.hashCode(title));
        return result;
    }
}
