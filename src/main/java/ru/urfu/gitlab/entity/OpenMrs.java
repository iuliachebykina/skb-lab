package ru.urfu.gitlab.entity;

import lombok.*;
import org.hibernate.Hibernate;
import ru.urfu.gitlab.idClass.OpenMrId;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@IdClass(OpenMrId.class)
public class OpenMrs {
    @Id
    private Integer projectId;
    @Id
    private Integer openMergeRequestIid;
    private Integer reviewerId;

    public OpenMrs(Integer projectId, Integer openMergeRequestIid, Integer reviewerId){
        this.projectId = projectId;
        this.openMergeRequestIid = openMergeRequestIid;
        this.reviewerId = reviewerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        OpenMrs that = (OpenMrs) o;

        if (!Objects.equals(projectId, that.projectId)) return false;
        return Objects.equals(openMergeRequestIid, that.openMergeRequestIid);
    }

    @Override
    public int hashCode() {
        int result = Objects.hashCode(projectId);
        result = 31 * result + (Objects.hashCode(openMergeRequestIid));
        return result;
    }
}
