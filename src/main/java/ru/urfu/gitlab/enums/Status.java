package ru.urfu.gitlab.enums;

public enum Status {
    AVAILABLE,
    OVERBURDENED,
    NOT_AVAILABLE
}
